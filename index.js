var express = require('express');
var fs = require('fs');
var path = require('path');
var config = JSON.parse(fs.readFileSync((process.cwd()) + "/config.json", "utf-8"));
var app = express();
var current_item = ""
var server = require('http').createServer(app);
var io = require('socket.io')(server);

//console colors
var cReset = "\x1b[0m",
    cBright = "\x1b[1m",
    cDim = "\x1b[2m",
    cUnderscore = "\x1b[4m",
    cBlink = "\x1b[5m",
    cReverse = "\x1b[7m",
    cHidden = "\x1b[8m",
    FgBlack = "\x1b[30m",
    FgRed = "\x1b[31m",
    FgGreen = "\x1b[32m",
    FgYellow = "\x1b[33m",
    FgBlue = "\x1b[34m",
    FgMagenta = "\x1b[35m",
    FgCyan = "\x1b[36m",
    FgWhite = "\x1b[37m",
    BgBlack = "\x1b[40m",
    BgRed = "\x1b[41m",
    BgGreen = "\x1b[42m",
    BgYellow = "\x1b[43m",
    BgBlue = "\x1b[44m",
    BgMagenta = "\x1b[45m",
    BgCyan = "\x1b[46m",
    BgWhite = "\x1b[47m";

server.listen(config.port, function () {
  console.log(FgGreen+'Server listening on port: '+ FgWhite + config.port + cReset);
});


app.use(express.static(__dirname + '/web'));
app.use(express.static(path.join(__dirname, '/../Systems')))

var numClients = 0;
var clientList = {};



io.sockets.on('connection', function (socket) {
  var newClient = false;
console.log("connection opened");
  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
      clientName: socket.clientName,
      message: data
    });
  });



  socket.on('sendItem', function(data, callback) {
    current_item = data;
    callback('Item Recieved!');
      console.log(data);
      socket.broadcast.emit('displayItem', {
        system: data["system"],
        rom: data["rom"]
      });
  });

  socket.on('currentItem', function(){
    console.log("sending current item to", socket.id);
     io.to(socket.id).emit('displayItem', current_item);
  });

   // socket.on("tvStart", function(target){
   //   var targetClient = clientList[target].id;
   //   io.to(targetClient).emit('tvStart', 'direct message to '+targetClient);
   // });
   // socket.on("tvStop", function(target){
   //   var targetClient = clientList[target].id;
   //   io.to(targetClient).emit('tvStop', 'direct message to '+targetClient);
   // });
   // socket.on("tvRestart", function(target){
   //   var targetClient = clientList[target].id;
   //   io.to(targetClient).emit('tvRestart', 'direct message to '+targetClient);
   // });
   // socket.on('getClients', function(clients){
   //   console.log("web requested Clients");
   //   clients(clientList);
   // });
    //    socket.on('getStatus', function(target, status){
    //      console.log("web requested status of",target);
    //     var targetClient = clientList[target].id;
    //        io.sockets.connected[targetClient].emit('getStatus', function(clientStatus){
    //          status(clientStatus);
    //        });
    //     io.to(targetClient).emit('getStatus', function(clientStatus){
    //         status(clientStatus);
    //     });
    //    });
  // when the client emits 'add user', this listens and executes
  socket.on('clientName', function (clientName) {
    if (newClient) return;
    clientList[clientName] = {id:socket.id};
    // we store the clientName in the socket session for this client
    socket.broadcast.emit('newClient', clientName);
    socket.clientName = clientName;
    io.to(socket.id).emit('console msg', 'direct message to '+clientName);
    console.log(socket.id);
    ++numClients;
    newClient = true;
    socket.emit('login', "Total Clients: "+numClients
    );
    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('client joined', {
      clientName: socket.clientName,
      'Total clients': numClients
    });
  });

  socket.on('clientMsg', function(msg){
    console.log("[CLIENT MSG]:", msg);
  })
  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    if (newClient) {
      --numClients;
      delete clientList[socket.clientName];
      // echo globally that this client has left
      socket.broadcast.emit('removeClient', socket.clientName);
      socket.broadcast.emit('user left', {
        clientName: socket.clientName,
        numClients: numClients
      });
    }
  });
});
